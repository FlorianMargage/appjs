const express = require("express");
const app = express();

const deuxTech = {
    tristan: "Je kiffe Symfony2",
    romain: "J'ai plutôt un profil réseaux",
    valentin: "La catho me manque",
    benoit: "L'info c'est un peu comme le droit...",
}

app.get("/ping", (req, res) => {
    res.send("Pong")
});

app.get("/eleves/:prenom", (req, res) => {
    const prénom = req.params.prenom;
    const élève = deuxTech[prénom];

    if(élève) {
        return res.json(élève);
    } else {
        res.status(404);
        res.json({ message: "L'élève n'existe pas!!" });
    }
});

app.listen(1234, () => console.log("listenning..."));